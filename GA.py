import random
import numpy as np
from population import *
import math
class GA:
    def __init__(self,W,H,isProducer=False,
                 Population_size=100
                 ,Pooling_size=1000
                 ,crossing_over_rate=0.5,
                 mutation_rate=0.2,
                 space_limitation_rate=0.3
                 ):
        self.W=W
        self.H=H
        self.Population=[]
        self.Pooling=[]
        self.isProducer=isProducer
        self.Population_size=Population_size
        self.Pooling_size=Pooling_size
        self.crossing_over_rate=crossing_over_rate
        self.space_limitation_rate=space_limitation_rate
        self.mutation_rate=mutation_rate
        for i in range(self.Population_size):
            if(self.isProducer):
                np=Producer(self.W,self.H)
                np.findFitness()
                self.Population.append(np)
            else:
                np=Consumer(self.W,self.H)
                np.findFitness()
                self.Population.append(np)
    def getFitness(self):
        maxFitness=-999999999999999999999
        minFitness=999999999999999999999
        sumFitness=0
        for p in self.Population:
            maxFitness=max(minFitness,p.fitness)
            minFitness=min(maxFitness,p.fitness)
            sumFitness+=p.fitness
        avgFitness=sumFitness/len(self.Population)
        return minFitness,maxFitness,sumFitness,avgFitness
    def getMaxFinessIndex(self):
        maxFitness=-999999999999999999999
        best_index=0
        for i in range(len(self.Population)):
            if(self.Population[i].fitness<maxFitness):
                maxFitness=self.Population[i].fitness
                best_index=i
        return best_index
    def DoPooling(self):
        self.Pooling=[]
        sum_pop=0
        self.Population.sort(key=lambda x:x.fitness)
        self.Population=self.Population[:int(len(self.Population)*0.80)]
        for p in self.Population:
            sum_pop+=(1000000/p.fitness)
        
        for p in self.Population:
            t=int((((1000000/p.fitness)/sum_pop))*self.Pooling_size)
            for i in range(t):
                self.Pooling.append(p)
        self.Pooling=random.sample(self.Pooling,len(self.Pooling))
        self.Population=self.Population[:int(len(self.Population)*0.05)]
    def CrossingOver(self):
        while(len(self.Population)<self.Population_size):
            #print(i)
            tempcity=np.zeros([self.H,self.W],dtype=np.int32)
            [c1,c2,c3,c4]=random.sample(self.Pooling,4)
            if(c1.fitness<c2.fitness):
                fatherDNA=c1
            else:
                fatherDNA=c2
            if(c3.fitness<c4.fitness):
                motherDNA=c3
            else:
                motherDNA=c4
            fatherDNA,motherDNA=fatherDNA.Pos,motherDNA.Pos
            sonDNA=fatherDNA
            for x,y,_ in sonDNA:
                tempcity[y,x]=1
            for index in range(len(sonDNA)):
                if(tempcity[motherDNA[index ][1],motherDNA[index ][0]] or (float(random.randint(0,100)/100.0))<=self.crossing_over_rate):
                    continue
                else:
                    
                    tempcity[motherDNA[index ][1],motherDNA[index ][0]]=1
                    tempcity[fatherDNA[index][1],fatherDNA[index][0]]=0
                    #print("Cross Over diff:",sonDNA[index][0]," ",sonDNA[index][1],end=" ")
                    sonDNA[index]=(motherDNA[index][0],motherDNA[index ][1],motherDNA[index ][2])
                    #print(sonDNA[index][0]," ",sonDNA[index][1])
            if(self.isProducer):
                self.Population.append(Producer(self.W,self.H,sonDNA))
            else:
                self.Population.append(Consumer(self.W,self.H,sonDNA)) 
        self.Pooling=[]
    def Mutation(self):
        for i in len(self.Population):
            tempcity=np.zeros([self.H,self.W],dtype=np.int32)
            for x,y,_ in self.Population[i].Pos:
                tempcity[y,x]=1
            for x,y,index in self.Population[i].Pos:
                if((float(random.randint(0,100)/100.0))>self.mutation_rate):
                    continue
                
                tempcity[y,x]=0
                new_x=random.randint(0,self.W-1)
                new_y=random.randint(0,self.H-1)
                while(tempcity[new_y,new_x]):
                    new_x=random.randint(0,self.W-1)
                    new_y=random.randint(0,self.H-1)
                #print("Mutation diff:",x," ",y," to ",new_x," ",new_y)
                self.Population[i].Pos[index]=(new_x,new_y,index)
                tempcity[new_y,new_x]=1
    def SpaceLimitation(self):
        for i in len(self.Population):
            tempcity=np.zeros([self.H,self.W],dtype=np.int32)
            for x,y,_ in self.Population[i].Pos:
                tempcity[y,x]=1
            for x,y,index in self.Population[i].Pos:
                if((float(random.randint(0,100)/100.0))>=self.space_limitation_rate):
                    continue
                else:
                    tempcity[y,x]=0
                    [new_x,new_y]=self.Population[-1].findBestPoints(x,y)
                    self.Population[i].city[new_y,new_x]=1
                    self.Population[i].Pos[index]=tuple([new_x,new_y,index])
            



















            
        