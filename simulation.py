import pygame
import numpy as np
from GA import *
import matplotlib.pyplot as plt
class simulation:
    def __init__(self,W,H,W_GRID,H_GRID,N,isSimP=True):
        self.W=W
        self.H=H
        self.W_GRID=W_GRID
        self.H_GRID=H_GRID
        self.W_S=int(W/W_GRID)
        self.H_S=int(H/H_GRID)
        self.N=N
        self.running=False
        self.bestFitnessIndex=0
        
        if(isSimP==True):
            self.f=open("./Result/GA/P/demofile.csv", "w")
            self.f.write("STEP,Min Fitness,Max Fitness,Sum FitnesssumFitness,Average Fitness\n")
            self.P=GA(100,100,isProducer=True,Population_size=60)
        else:
            self.f=open("./Result/GA/C/demofile.csv", "w")
            self.f.write("STEP,Min Fitness,Max Fitness,Sum FitnesssumFitness,Average Fitness\n")
            self.P=GA(100,100,isProducer=False,Population_size=60)
        
    def run(self):
        pygame.init()
        pygame.display.set_caption("City Planning")
        self.screen=pygame.display.set_mode((self.W,self.H))
        self.running=True
        step=1
        while(self.running):
            print("STEP",step,":")
            
            self.draw()
            minFitness,maxFitness,sumFitness,avgFitness=self.P.getFitness()
            print("Min Fitness:",minFitness,
                  "\nMax Fitness:",maxFitness,
                  "\nSum Fitness:",sumFitness,
                  "\nAverage Fitness:",avgFitness)
            self.f.write(str(step)+','+str(minFitness)+','+str(maxFitness)+','+str(sumFitness)+','+str(avgFitness)+'\n')
            pygame.image.save(self.screen,"./Result/GA/C/IMAGES/"+str(step)+'.png')
            self.update()
            self.evnt()
            
            #pygame.time.Clock().tick(60)
            step+=1
        self.f.close()
        pygame.quit()
    def evnt(self):
        for evnts in pygame.event.get():
            if(evnts.type==pygame.QUIT):
                self.running=False
    def update(self):
        self.P.DoPooling()
        self.P.CrossingOver()
        self.Mutation()
        self.SpaceLimitation()
        for i in len(self.P):
            new_P=self.P[i]
            new_P.findFitness()
            self.P[i]=new_P
        self.bestFitnessIndex=self.P.getMaxFinessIndex()
    def draw(self):
        self.screen.fill((186, 207, 242))
        MAP=self.P.Population[self.bestFitnessIndex]
        
        for i in range(self.H_GRID):
            for j in range(self.W_GRID):
                
                if(MAP.city[i,j]==1):
                    color=(255, 216, 230)
                elif(MAP.city[i,j]==7):
                    color=(244, 172, 65)
                elif(MAP.city[i,j]==14):
                    color=(199, 244, 65)
                elif(MAP.city[i,j]==21):
                    color=(84, 232, 0)
                elif(MAP.city[i,j]==28):
                    color=(0, 232, 212)
                elif(MAP.city[i,j]==35):
                    color=(0, 143, 232)
                elif(MAP.city[i,j]==42):
                    color=(88, 0, 232)
                elif(MAP.city[i,j]==49):
                    color=(204, 0, 232)
                elif(MAP.city[i,j]==56):
                    color=(232, 0, 162)
                elif(MAP.city[i,j]==63):
                    color=(232, 0, 104)
                else:
                    color=(0, 0, 0)
                
                pygame.draw.rect(self.screen,color,[j*self.W_S,i*self.H_S
                                                 ,j*self.W_S+self.W_S,i*self.H_S+self.H_S])
        for i in range(0,self.H,self.H_S):
            pygame.draw.line(self.screen,(0, 0, 0),(0,i),(self.W,i),1)
        for i in range(0,self.W,self.W_S):
            pygame.draw.line(self.screen,(0, 0,0),(i,0),(i,self.H),1)
        pygame.display.update()
                


















                
        